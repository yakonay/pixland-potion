package ru.pixland.potion.command.api;

import org.bukkit.plugin.Plugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
/**
 * Класс взят из kaytato api :>
 * (old: jamesapi)
 *
 * @author: vk.com/kaytato
 */
public abstract class kCommand<S extends CommandSender>
        extends Command
        implements CommandExecutor {

    private boolean onlyPlayers = false;

    public kCommand(String command) {
        this(command, new String[0]);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        return true;
    }

    public kCommand(String command, String... aliases) {
        super(command, "The JamesAPI command. Thank for using.", ("/").concat(command), Arrays.asList(aliases));
        this.onlyPlayers = ((Class<S>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0]).isAssignableFrom(Player.class);
    }

    protected abstract void handleExecute(S sender, String[] args);

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        if (onlyPlayers && !(commandSender instanceof Player)) {
            commandSender.sendMessage("Upps.. This command only players, sorry c:");
            return true;
        }
        handleExecute((S) commandSender, args);
        return true;
    }

    public void register(Plugin plugin){
        try {
            CommandManager.registerCommand(plugin, this, getName()
                    , getAliases().toArray(new String[0]));
        } catch(Exception proebaliSuccess) {
        }
    }

}
