package ru.pixland.potion.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import ru.pixland.potion.event.PlayerDamageByPlayerEvent;
import ru.pixland.potion.potion.Potion;

public class PotionListener implements Listener {

    @EventHandler
    public void onDamage(PlayerDamageByPlayerEvent event) {
        Player damager = event.getDamager();
        Player target = event.getTarget();


        if (Potion.POTION_PLAYER_MAP.get(damager).getOnPlayerDamaged() != null) {
            Potion.POTION_PLAYER_MAP.get(damager).getOnPlayerDamaged().accept(event);
        }
    }
}
